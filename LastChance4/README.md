# Task: Emulate LED Strip using 3 arduino uno

Each controller is connected to the next arduino and LED.

Arduino uno takes 3 bytes of data, sends it to the LED and send all other data to the next arduino.

Each arduino has the same code.

## Scheme:

![LastChance4.png](../raw/LastChance4.png)

## Project on tinkercad:

[Tinkercad | From mind to design in minutes](https://www.tinkercad.com/things/8gdKKmeaVjm-lastchance4/editel?sharecode=pi4lWedIQbgjFJroBcJyt7WmnR4ZSBBo2m1WIO4RMU0)
