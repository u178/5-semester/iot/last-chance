#include <SoftwareSerial.h>


SoftwareSerial mySerial(10, 11); // RX, TX
void setup() {
  pinMode(10, INPUT);
  pinMode(11, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  
  Serial.begin(4800);

  while (!Serial) {
  }


  mySerial.begin(4800);
  delay(100);
}



void loop() {
  
  if (Serial.available() >= 3) {

    uint8_t r = Serial.read();
    uint8_t g = Serial.read();
    uint8_t b = Serial.read();
    
    analogWrite(3, g);
    analogWrite(5, b);
    analogWrite(6, r);

    while (Serial.available() > 0) {
      mySerial.write(Serial.read());
    } 
  }
  
  delay(100);
}