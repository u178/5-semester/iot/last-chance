#include <SoftwareSerial.h>


SoftwareSerial mySerial(10, 11); // RX, TX
void setup() {
  Serial.begin(57600);

  while (!Serial) {
  }

  Serial.println("hi 1!");
  mySerial.begin(4800);
  mySerial.println("Hello, world?");
  Serial.println("Send hello world to 2nd!");
  Serial.println("Hello, world?");
}


void loop() { // run over and over
  uint8_t a1 = analogRead(A1) /4;
  uint8_t a2 = analogRead(A2) /4;
  uint8_t a3 = analogRead(A3) /4;
  uint8_t a4 = analogRead(A4) /4;
  uint8_t a5 = analogRead(A5) /4;
  
  Serial.println(a1);
  mySerial.write(a1);
  Serial.println(a2);
  mySerial.write(a2);
  Serial.println(a3);
  mySerial.write(a3);
  Serial.println(a4);
  mySerial.write(a4);
  Serial.println(a5);
  mySerial.write(a5);
  Serial.println("----");
  

  delay(1000);
}