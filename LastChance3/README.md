# Task: connect 10 analog sensors to 1 arduino using additional arduino

5 sensors are connected directly to the main arduino, another 5 are connected to the dependent arduino and then trasmit data via SoftwareSerial.

## Scheme:

![LastChance3.png](../raw/LastChance3.png)

## Project on tinkercad:

[Tinkercad | From mind to design in minutes](https://www.tinkercad.com/things/37CfVlBxrvM-lastchance3/editel?sharecode=l9izG2dhzBROqw9lKe1gQ5JJuzFdxJphCiAseb19wTE)
